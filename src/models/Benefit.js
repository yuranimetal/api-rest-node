const mongoose = require('mongoose');
const { Schema } = mongoose;

const BenefitSchema = new Schema ({
    name: {type: String, require:true},
    shortDescription: {type: String},
    longDescription: {type: String},
    image: {type: String},
    icon: {type: String}
});

module.exports = mongoose.model('daservicios', BenefitSchema);