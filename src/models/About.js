const mongoose = require('mongoose');
const { Schema } = mongoose;

const aboutSchema = new Schema ({
    mission: {type: String},
    vision: {type: String},
    description: {type: String},
    ourStrategy: {type: String},
    funtion1: {type: String},
    funtion2: {type: String}
});

module.exports = mongoose.model('abouts', aboutSchema);