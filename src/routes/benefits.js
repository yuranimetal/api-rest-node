const router = require('express').Router();
const Daservicios = require('../models/Benefit');

//ROUTE facilita la creación de rutas


router.get('/', async(req, res) => {
    const daservicios = await Daservicios.find().sort({date:'asc'});
    console.log(daservicios);
    res.json(daservicios);
});


module.exports = router;