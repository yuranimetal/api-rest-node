const router = require('express').Router();
const About = require('../models/About');

//ROUTE facilita la creación de rutas

router.get('/', async(req, res) => {
    const about = await About.find().then(result => {
        res.json(result);
        res.end();
    }).catch ( err => {
        console.log(err);
        res.end();
    });
});


module.exports = router;