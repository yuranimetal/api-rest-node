const express = require('express');
const path = require('path');
const handlebars =  require('express-handlebars');
const methodOverride = require('method-override');
const session = require('express-session');
const Handlebars = require('handlebars');
const {allowInsecurePrototypeAccess} = require('@handlebars/allow-prototype-access');
const flash = require('connect-flash');
const cors = require('cors');

// Initializatons
const app = express();

require('./database');

// settings
app.use(cors());
app.set('port', process.env.PORT ||  4000);
app.set('views', path.join(__dirname, 'views')); // join permite unir directorios esdecir si ejecuto index
//me deveulve src y al evolverme ese src yo lo puedo unir con otras por ejemplo con views
app.engine('.hbs',handlebars({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs',
    handlebars: allowInsecurePrototypeAccess(Handlebars)
}) );
app.set('view engine', '.hbs');

// Middleware: funciones antes que lleguen al servidor
app.use(express.urlencoded({extended: false}));
app.use(methodOverride('_method'));
app.use(session({
    secret: 'mysecretapp',
    resave: true, 
    saveUninitialized: true
}));
app.use(flash());

// Global Variables
app.use((req,res,next) => {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    next();
})

// Routes
app.use('/api/benefit',require('./routes/benefits'));
app.use('/api/about',require('./routes/about'));

// Static Files
app.use(express.static(path.join(__dirname,'public')));

// Server is listenning
app.listen(app.get('port'), () => {
    console.log('Servidor corriendo en el puerto ', app.get('port'));
});